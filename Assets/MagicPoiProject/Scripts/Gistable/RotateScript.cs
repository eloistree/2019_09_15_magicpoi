﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateScript : MonoBehaviour
{
    public Vector3 m_rotate;
    public float m_speed=1;

    // Update is called once per frame
    void Update()
    {
        transform.Rotate(new Vector3(Random.Range(0,m_rotate.x), Random.Range(0, m_rotate.y), Random.Range(0, m_rotate.z) ) * Time.deltaTime*m_speed, Space.Self);
    }

    private void Reset()
    {
        m_rotate = new Vector3(Random.value * 360, Random.value * 360, Random.value * 360);
    }
}
