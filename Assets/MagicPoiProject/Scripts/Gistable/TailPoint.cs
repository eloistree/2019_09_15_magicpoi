﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TailPoint : MonoBehaviour
{
    public LineRenderer m_lineRenderer;

    public Queue<Vector3> m_positions=new Queue<Vector3>();
    public int m_maxPoint=10;
    public float m_timeBetweenRecord = 0.1f;
    public static List<TailPoint> inScene = new List<TailPoint>();

    void Start()
    {
        inScene.Add(this);
        InvokeRepeating("AddPoint", 0, m_timeBetweenRecord);
    }

    void AddPoint()
    {
        m_positions.Enqueue(transform.position);
        if (m_positions.Count > m_maxPoint) 
        m_positions.Dequeue();
    }

    public void Update()
    {
        List<Vector3> trail = new List<Vector3>();
        trail.AddRange(m_positions);
        trail.Add(transform.position);
        Vector3[] arryOfPoints = trail.ToArray();
        m_lineRenderer.positionCount= (arryOfPoints.Length);
        m_lineRenderer.SetPositions(arryOfPoints);
    }

    internal void ResetPoints()
    {
        m_positions.Clear();
    }
}
