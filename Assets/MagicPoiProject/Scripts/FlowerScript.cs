﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlowerScript : MonoBehaviour
{
    private void Start()
    {
        RaycastHit hit;

        if (Physics.Raycast(transform.position,-transform.up,out hit,500))
        {
            if (hit.collider.CompareTag("Ground"))
            {
                transform.position = hit.point;
            }
        }
    }
}
