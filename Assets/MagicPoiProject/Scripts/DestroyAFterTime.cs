﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyAFterTime : MonoBehaviour
{
    public float time=5f;
    // Start is called before the first frame update
    void Start()
    {
        Invoke("Kill", time);
    }

    // Update is called once per frame
    void Kill()
    {
        Destroy(this.gameObject);
    }
}
