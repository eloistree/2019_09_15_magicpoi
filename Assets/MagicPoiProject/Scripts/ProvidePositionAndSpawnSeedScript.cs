﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProvidePositionAndSpawnSeedScript : MonoBehaviour
{
    public float m_speedUpDistance;
    public GameObject m_seed;

    private List<Vector3> _tailQ = new List<Vector3>();

    // Update is called once per frame
    void Update()
    {
        ProvidePos();
    }

    public void ProvidePos()
    {
        if (_tailQ.Count <= 20)
        {
            _tailQ.Add(transform.position);
        }
        else
        {
            if ((Vector3.Distance(_tailQ[_tailQ.Count - 2],_tailQ[_tailQ.Count - 1])) > m_speedUpDistance)
            {
                GameObject seed = Instantiate(m_seed, transform.position, Quaternion.identity);
                seed.GetComponent<Rigidbody>().AddForce(CalculeForceDirection(),ForceMode.Impulse);
            }

            _tailQ.Clear();
        }

    }

    public Vector3 CalculeForceDirection()
    {
        Vector3 result1 = (_tailQ[_tailQ.Count - 1] - _tailQ[_tailQ.Count - 2]) + transform.position;
        Vector3 result2 = (_tailQ[_tailQ.Count - 1] - _tailQ[_tailQ.Count - 3]) + transform.position;
        Vector3 result3 = (_tailQ[_tailQ.Count - 1] - _tailQ[_tailQ.Count - 4]) + transform.position;

        Vector3 avgPos = ((result1 + result2 + result3) / 3);

        return avgPos;
    }
}


public class ResetBox : MonoBehaviour {

    public TailPoint [] m_tailReset;
    private void OnCollisionEnter(Collision collision)
    {
        ResetPoint(TailPoint.inScene);



    }

    private void ResetPoint(List<TailPoint> inScene)
    {
        foreach (var item in inScene)
        {
            ResetPoint(item);
        }
    }
    private void ResetPoint(TailPoint inScene)
    {
        inScene.ResetPoints();
    }
}